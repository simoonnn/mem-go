package getmsgs

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
	initial "../getinitial"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	ChatWith string `json:"chat_with"`
	Offset   int    `json:"offset"`
}

type response struct {
	Status   string            `json:"status"`
	Messages []initial.Message `json:"messages"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// initialize response
		var res response

		// decode the body
		var req request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Is logged in
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			res.Status = "notlogged"
		} else {
			// search chats
			id := initial.ChatID(req.Nickname, req.ChatWith, conn)

			// chat exists
			if id != -1 {
				res.Status = "chatexists"

				// getting messages
				res.Messages = initial.MsgBytes(id, req.Nickname, req.ChatWith, req.Offset, conn)
			} else { // chat doesn't exist
				res.Status = "chaterror"
			}

		}

		// encode response
		var bs []byte
		bs, err = json.Marshal(res)
		if err != nil {
			log.Println(err)
		}
		w.Write(bs)

	}
	return handle
}
