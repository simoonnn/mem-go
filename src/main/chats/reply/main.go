package reply

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type response struct {
	Status string `json:"status"`
	ID     uint16 `json:"id,omitempty"`
}

type request struct {
	Nickname  string `json:"nickname"`  // The author
	Recipient string `json:"recipient"` // The recipient
	Token     string `json:"token"`
	ID        uint16 `json:"id"`
	Value     string `json:"value"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var res response
		// Decode the body
		var req request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Is logged in?
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			res.Status = "notlogged"
		} else {
			// edit the message
			id := replyToMessage(req.ID, req.Value, req.Nickname, req.Recipient, conn)
			res.ID = id
		}

		var bytes []byte
		bytes, err = json.Marshal(res)
		if err != nil {
			log.Println(err)
		}

		w.Write(bytes)
	}
	return handle
}
