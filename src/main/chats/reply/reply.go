package reply

import (
	"database/sql"
	"log"
	"time"

	initial "../getinitial"
)

func replyToMessage(id uint16, val string, author string, rec string, conn *sql.DB) uint16 {
	var msgid uint16
	epoch := time.Now().UnixNano() / (1000 * 1000)
	chatID := initial.ChatID(author, rec, conn)

	query := "INSERT INTO messages (reply_to, to_person, owner, contents, date, chat_id) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"

	err := conn.QueryRow(query, id, rec, author, val, epoch, chatID).Scan(&msgid)
	if err != nil {
		log.Println(err)
	}

	return msgid
}
