package searchexisting

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"log"

	"../../presets"
	"../getchats"
)

func getChats(l string, n string, c *sql.DB) []byte {
	// temporary chat
	var tc getchats.Chat

	// temporary owner and to_person
	var tt string
	var to string
	var ttd bool
	var tod bool

	res := make([]getchats.Chat, 0, presets.CPRWS)

	// Selecting chats
	q := "WITH t AS (SELECT MAX(id) as id FROM messages WHERE chat_id IN (SELECT id FROM chats WHERE (user_1=$1 AND LOWER(user_2) LIKE $2) OR (user_2=$1 AND LOWER(user_1) LIKE $2)) GROUP BY chat_id) SELECT m.contents, m.date, m.to_person, m.owner, del, owner_deleted, to_person_deleted from messages as m, t WHERE m.id = t.id ORDER BY m.id DESC LIMIT $3"

	// Concatenate %, string and %
	var buf bytes.Buffer
	buf.WriteRune('%')
	buf.WriteString(l)
	buf.WriteRune('%')
	l = buf.String()

	rows, err := c.Query(q, n, l, presets.CPRWS)
	if err != nil {
		log.Println(err)
	}

	// Scan through rows and append to res
	for rows.Next() {
		err = rows.Scan(&tc.Contents, &tc.Date, &tt, &to, &tc.Deleted, &tod, &ttd)
		if err != nil {
			log.Println(err)
		}

		// Decide what the needed nickname is
		if tt == n {
			tc.Nickname = to
		} else if to == n {
			tc.Nickname = tt
		}

		// Get rid of the contents if the message is deleted

		if !tc.Deleted {
			// to_person equal to nickname
			if tt == n {
				if ttd {
					tc.Deleted = true
					tc.Contents = ""
				}
			} else {
				if tod {
					tc.Deleted = true
					tc.Contents = ""
				}
			}
		}

		// Cut down the contents if they are longer than needed
		if len(tc.Contents) > presets.MaxCont {
			tc.Contents = tc.Contents[0:presets.MaxCont]
		}

		// Append to res
		res = append(res, tc)
	}

	// marshal the response
	var bytes []byte
	bytes, err = json.Marshal(res)
	if err != nil {
		log.Println(err)
	}
	return bytes
}
