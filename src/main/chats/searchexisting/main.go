package searchexisting

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
	"../search"
)

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// Unmarshall the body
		var req search.Request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Check if logged in
		islogged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !islogged {
			w.Write([]byte("notlogged"))
			return
		}

		// get the bytes
		bytes := getChats(req.Like, req.Nickname, conn)
		w.Write(bytes)

	}

	return handle
}
