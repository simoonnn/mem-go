package editmsg

import (
	"database/sql"
	"log"
)

func editMessage(id uint16, value string, owner string, conn *sql.DB) {
	q := "UPDATE messages SET contents=$1, type='updated' WHERE id=$2 AND owner=$3"
	stm, err := conn.Prepare(q)
	if err != nil {
		log.Println(err)
	}

	_, err = stm.Exec(value, id, owner)
	if err != nil {
		log.Println(err)
	}
}
