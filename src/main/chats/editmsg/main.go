package editmsg

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	ID       uint16 `json:"id"`
	NewValue string `json:"value"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// Decode the body
		var req request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Is logged in?
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			w.Write([]byte("notlogged"))
			return
		}

		// edit the message
		editMessage(req.ID, req.NewValue, req.Nickname, conn)
		w.Write([]byte("success"))
	}
	return handle
}
