package checkmessages

import (
	"database/sql"
	"log"

	initial "../getinitial"
)

type message struct {
	initial.Message
	Type string `json:"type"`
}

func getMessages(chatID int, chatWith string, c *sql.DB) []message {
	msgs := make([]message, 0, 100)
	var s message

	// Select messages, using offset
	q := `SELECT m.id, m.date, m.contents, m.image_url, m.owner, m.to_person, m.del, m.type,
	r.date rdate, r.contents rcontents, r.image_url rimage_url, r.id rid, r.del rdel, r.owner rowner, r.to_person rto_person
	 FROM messages as m LEFT JOIN messages as r
	  ON m.reply_to=r.id
		   WHERE m.chat_id=$1 AND m.type != 'normal' AND m.owner=$2
		   ORDER BY id DESC`

	var rows *sql.Rows
	rows, err := c.Query(q, chatID, chatWith)
	if err != nil {
		log.Println(err)
	}
	for rows.Next() {
		err = rows.Scan(&s.ID, &s.Date, &s.Contents, &s.ImageURL, &s.Owner, &s.ToPerson, &s.Deleted, &s.Type, &s.RepliedDate, &s.RepliedContents, &s.RepliedImageURL, &s.RepliedID, &s.RepliedDeleted, &s.RepliedOwner, &s.RepliedToPerson)
		if err != nil {
			log.Println(err)
		}

		msgs = append(msgs, s)
	}

	if len(msgs) != 0 {
		// set all of the types to normal
		q = "UPDATE messages SET type='normal' WHERE owner=$1 AND chat_id =$2 AND type != 'normal'"
		var stm *sql.Stmt
		stm, err = c.Prepare(q)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(chatWith, chatID)
		if err != nil {
			log.Println(err)
		}
	}

	return msgs
}
