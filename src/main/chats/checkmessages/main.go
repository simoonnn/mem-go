package checkmessages

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
	initial "../getinitial"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	ChatWith string `json:"chat_with"`
}

type response struct {
	Status   string    `json:"status"`
	Messages []message `json:"messages"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// decode the body
		var req request
		var res response
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// is logged in
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			res.Status = "notlogged"
		} else {
			// get chat id
			ci := initial.ChatID(req.Nickname, req.ChatWith, conn)
			if ci == -1 {
				res.Status = "chatdoesntexist"
			} else {
				res.Status = "success"
				res.Messages = getMessages(ci, req.ChatWith, conn)
			}
		}

		var bytes []byte
		bytes, err = json.Marshal(res)
		w.Write(bytes)
	}
	return handle
}
