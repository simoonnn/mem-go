package delmsg

import (
	"database/sql"
	"log"
)

func deleteMessage(nickname string, id uint16, c *sql.DB) {
	// delete the message from the database
	q := "UPDATE messages SET del=true, contents='', type='updated' WHERE id=$1 AND owner=$2"
	stm, err := c.Prepare(q)
	if err != nil {
		log.Println(err)
	}

	_, err = stm.Exec(id, nickname)
	if err != nil {
		log.Println(err)
	}
}
