package wipemsg

import (
	"database/sql"
	"fmt"
	"log"
)

func wipeMessage(nickname string, id uint16, c *sql.DB) {
	// Select owner from messages with the known id
	var o string
	q := "SELECT owner FROM messages WHERE id=$1"
	err := c.QueryRow(q, id).Scan(&o)
	if err != nil {
		log.Println(err)
	}

	// Decide what to change
	var column string
	if nickname == o {
		column = "owner_deleted"
	} else {
		column = "to_person_deleted"
	}

	// Construct the query
	q = "UPDATE messages SET %s=true WHERE id=$1"
	q = fmt.Sprintf(q, column)

	// Execute the statement
	var stm *sql.Stmt
	stm, err = c.Prepare(q)

	if err != nil {
		log.Println(err)
	}

	_, err = stm.Exec(id)
	if err != nil {
		log.Println(err)
	}
}
