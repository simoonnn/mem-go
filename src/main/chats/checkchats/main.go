package checkchats

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
	"../../utils/request"
	"../getchats"
)

type response struct {
	Status string          `json:"status"`
	Chats  []getchats.Chat `json:"chats"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var res response

		// decode the body
		var req request.Request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// is loggedin
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			res.Status = "notlogged"
		} else {
			// get chats
			res.Status = "success"
			res.Chats = getChats(req.Nickname, conn)
		}

		var bytes []byte
		bytes, err = json.Marshal(res)
		if err != nil {
			log.Println(err)
		}

		w.Write(bytes)
	}
	return handle
}
