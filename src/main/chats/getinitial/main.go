package get

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	ChatWith string `json:"chat_with"`
}

type response struct {
	Status   string    `json:"status"`
	Messages []Message `json:"messages"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// initialize response
		var res response

		// decode the body
		var req request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Is logged in
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			res.Status = "notlogged"
		} else {
			// search chats
			id := ChatID(req.Nickname, req.ChatWith, conn)

			// chat exists
			if id != -1 {
				res.Status = "chatexists"

				// getting messages
				res.Messages = MsgBytes(id, req.Nickname, req.ChatWith, 0, conn)
			} else { // chat doesn't exist
				createnewchat(req.Nickname, req.ChatWith, conn)
				res.Status = "newchat"
			}

		}

		// encode response
		var bs []byte
		bs, err = json.Marshal(res)
		if err != nil {
			log.Println(err)
		}
		w.Write(bs)

	}
	return handle
}
