package get

import (
	"database/sql"
	"log"

	"../../presets"
)

type nullString string

// Message holds message data
type Message struct {
	Date            int     `json:"date"`
	Contents        string  `json:"contents,omitempty"`
	ID              int     `json:"id"`
	ImageURL        string  `json:"image_url"`
	Owner           string  `json:"owner"`
	ToPerson        string  `json:"to_person"`
	Deleted         bool    `json:"deleted"`
	RepliedDate     *int    `json:"replied_date,omitempty"`
	RepliedContents *string `json:"replied_contents,omitempty"`
	RepliedID       *int    `json:"replied_id,omitempty"`
	RepliedImageURL *string `json:"replied_image_url,omitempty"`
	RepliedDeleted  *bool   `json:"replied_deleted,omitempty"`
	RepliedOwner    *string `json:"replied_owner,omitempty"`
	RepliedToPerson *string `json:"replied_person,omitempty"`
}

// MsgBytes gets bytes of messages
func MsgBytes(id int, nickname string, chatWith string, uo int, c *sql.DB) []Message {
	msgs := make([]Message, 0, presets.MESSAGELIMIT)
	var s Message

	// Select messages, using offset
	q := `WITH t as (SELECT m.id, m.date, m.contents, m.image_url, m.owner, m.to_person, m.del,
		 r.date rdate, r.contents rcontents, r.image_url rimage_url, r.id rid, r.del rdel, r.owner rowner, r.to_person rto_person
		  FROM messages as m LEFT JOIN messages as r
		   ON m.reply_to=r.id
		   WHERE m.chat_id=$1 AND
		  (m.owner=$4 AND m.owner_deleted=false OR m.to_person=$4 AND m.to_person_deleted=false) ORDER BY m.id DESC OFFSET $2 LIMIT $3)
		   SELECT * FROM t ORDER BY id;`

	var rows *sql.Rows
	rows, err := c.Query(q, id, uo, presets.MESSAGELIMIT, nickname)
	if err != nil {
		log.Println(err)
	}
	for rows.Next() {
		err = rows.Scan(&s.ID, &s.Date, &s.Contents, &s.ImageURL, &s.Owner, &s.ToPerson, &s.Deleted, &s.RepliedDate, &s.RepliedContents, &s.RepliedImageURL, &s.RepliedID, &s.RepliedDeleted, &s.RepliedOwner, &s.RepliedToPerson)
		if err != nil {
			log.Println(err)
		}

		msgs = append(msgs, s)
	}

	return msgs
}
