package get

import (
	"database/sql"
	"log"
)

func createnewchat(n string, w string, c *sql.DB) {
	q := "INSERT INTO chats (user_1, user_2) VALUES ($1, $2)"
	stm, err := c.Prepare(q)
	if err != nil {
		log.Println(err)
	}
	_, err = stm.Exec(n, w)
}
