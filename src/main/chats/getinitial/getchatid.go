package get

import (
	"database/sql"
	"log"
)

// ChatID gets the id of a chat
func ChatID(n string, r string, c *sql.DB) int {
	q := "SELECT id FROM chats WHERE (user_1=$1 AND user_2=$2) OR (user_1=$2 AND user_2=$1)"
	var id int
	err := c.QueryRow(q, n, r).Scan(&id)
	if err == sql.ErrNoRows {
		return -1
	}
	if err != nil {
		log.Println(err)
	}
	return id
}
