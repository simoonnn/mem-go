package open

import (
	"database/sql"
	"encoding/json"
	"log"
)

type user struct {
	AvatarURL string `json:"avatar_url"`
	Nickname  string `json:"nickname"`
}

func getChats(n string, c *sql.DB) []byte {
	var tu user
	// result
	r := make([]user, 0, 15)

	q := "SELECT avatar_url, owner FROM users_info WHERE owner IN (WITH t AS (SELECT subscribed FROM subscriptions WHERE to_person=$1 UNION SELECT to_person FROM subscriptions WHERE subscribed=$1) SELECT * FROM t);"
	rows, err := c.Query(q, n)
	if err != nil {
		log.Println(err)
	}

	// iterate through rows, append to r
	for rows.Next() {
		err = rows.Scan(&tu.AvatarURL, &tu.Nickname)
		if err != nil {
			log.Println(err)
		}
		r = append(r, tu)
	}

	var bytes []byte
	bytes, err = json.Marshal(r)

	return bytes

}
