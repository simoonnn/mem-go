package open

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		var req request
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Is logged in
		il := logged.IsLoggedIn(req.Nickname, req.Token, conn)

		// If not...
		if !il {
			w.Write([]byte("notlogged"))
			return
		}

		// If logged
		data := getChats(req.Nickname, conn)
		w.Write(data)
	}
	return handle
}
