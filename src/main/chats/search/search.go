package search

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"log"
)

type user struct {
	AvatarURL string `json:"avatar_url"`
	Nickname  string `json:"nickname"`
}

func searchChats(n string, like string, c *sql.DB) []byte {
	var tu user
	// result
	r := make([]user, 0, 15)

	// Concatenate %, Requested value and another %
	var buf bytes.Buffer
	buf.WriteRune('%')
	buf.WriteString(like)
	buf.WriteRune('%')

	like = buf.String()

	q := "SELECT avatar_url, owner FROM users_info WHERE LOWER(owner) LIKE LOWER($1) AND owner IN (WITH t AS (SELECT subscribed FROM subscriptions WHERE to_person=$2 UNION SELECT to_person FROM subscriptions WHERE subscribed=$2) SELECT * FROM t);"
	rows, err := c.Query(q, like, n)
	if err != nil {
		log.Println(err)
	}

	// iterate through rows, append to r
	for rows.Next() {
		err = rows.Scan(&tu.AvatarURL, &tu.Nickname)
		if err != nil {
			log.Println(err)
		}
		r = append(r, tu)
	}

	var bytes []byte
	bytes, err = json.Marshal(r)

	return bytes

}
