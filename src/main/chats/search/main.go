package search

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

// Request holds request info
type Request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	Like     string `json:"value"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		// initialize response
		var bs []byte
		SendCORSHeaders(w)

		// decode the body
		var req Request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println("CANNOT DECODE")
			log.Println(err)
		}

		// Is logged in
		l := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if l {
			bs = searchChats(req.Nickname, req.Like, conn)
		} else {
			bs = []byte("notlogged")
		}

		w.Write(bs)
	}
	return handle
}
