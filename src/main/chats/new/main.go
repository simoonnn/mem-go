package new

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"

	"../../utils/logged"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	ChatWith string `json:"chat_with"`
	Value    string `json:"value"`
}

type response struct {
	ID     int    `json:"id"`
	Status string `json:"status"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// initialize the response
		var res response

		// decode request body
		var req request
		dec := json.NewDecoder(r.Body)
		err := dec.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// is logged in
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			res.Status = "notlogged"
			return
		}

		// Check if the trimmed contents are good
		trimed := strings.TrimSpace(req.Value)
		if trimed == "" {
			res.Status = "invalid request"
		} else {
			if len(trimed) > 1200 {
				res.Status = "toolong"
			} else {
				// insert the message in the database
				date := time.Now().UnixNano() / (1000 * 1000)
				insert(req.Nickname, req.ChatWith, trimed, date, conn, &res)
			}
		}

		// marshall the response
		var bytes []byte
		bytes, err = json.Marshal(res)
		if err != nil {
			log.Println(err)
		}
		w.Write(bytes)
	}
	return handle
}
