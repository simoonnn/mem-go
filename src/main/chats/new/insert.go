package new

import (
	"database/sql"
	"log"

	get "../getinitial"
)

func insert(n string, r string, v string, d int64, c *sql.DB, resp *response) {
	id := get.ChatID(n, r, c)
	// If no such chat
	// Actually you can think about creating a new chat at this point
	if id == -1 {
		resp.Status = "unsuccess"
		return
	}

	// if chat_exists
	var msgID int
	query := "INSERT INTO messages (contents, date, chat_id, owner, to_person) VALUES ($1, $2, $3, $4, $5) RETURNING id"
	err := c.QueryRow(query, v, d, id, n, r).Scan(&msgID)
	if err != nil {
		log.Println(err)
	}

	resp.Status = "success"
	resp.ID = msgID
}
