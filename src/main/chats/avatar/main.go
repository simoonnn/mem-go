package avatar

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
)

type request struct {
	Nickname string `json:"nickname"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var req request

		// decode the body
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		bytes := getAvatar(req.Nickname, conn)
		w.Write(bytes)
	}

	return handle
}
