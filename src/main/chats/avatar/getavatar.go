package avatar

import (
	"database/sql"
	"encoding/json"
	"log"
)

type response struct {
	AvatarURL string `json:"avatar_url"`
}

func getAvatar(n string, c *sql.DB) []byte {
	var resp response

	// Get avatar_url
	var au string
	q := "SELECT avatar_url FROM users_info WHERE owner=$1"
	err := c.QueryRow(q, n).Scan(&au)
	if err == sql.ErrNoRows {
		resp.AvatarURL = ""
	} else if err != nil {
		log.Println(err)
	} else {
		resp.AvatarURL = au
	}

	// Marshal the thing
	var bytes []byte
	bytes, err = json.Marshal(resp)
	if err != nil {
		log.Println(err)
	}
	return bytes
}
