package getchats

import (
	"database/sql"
	"encoding/json"
	"log"

	"../../presets"
)

// Chat holds data of chat
type Chat struct {
	Nickname string `json:"nickname"`
	Date     string `json:"date"`
	Contents string `json:"contents"`
	Deleted  bool   `json:"deleted"`
	ChatID   int16  `json:"chat_id"`
}

func getChats(n string, c *sql.DB) []byte {
	var tu Chat

	// temporary owner, to_person, owner_deleted, to_person_deleted
	var to string
	var tt string
	var od bool
	var tpd bool

	// result
	r := make([]Chat, 0, presets.CPR)

	q := "WITH t AS (SELECT MAX(id) as id FROM messages WHERE chat_id IN (SELECT id FROM chats WHERE user_1=$1 OR user_2=$1) GROUP BY chat_id) SELECT m.contents, m.date, m.to_person, m.owner, m.owner_deleted, m.to_person_deleted, m.del, m.chat_id from messages as m, t WHERE m.id = t.id ORDER BY m.id DESC LIMIT $2"
	rows, err := c.Query(q, n, presets.CPR)
	if err != nil {
		log.Println(err)
	}

	// iterate through rows, append to r
	for rows.Next() {
		err = rows.Scan(&tu.Contents, &tu.Date, &tt, &to, &od, &tpd, &tu.Deleted, &tu.ChatID)
		if err != nil {
			log.Println(err)
		}
		// Decide what the needed nickname is
		if tt == n {
			tu.Nickname = to
		} else if to == n {
			tu.Nickname = tt
		}

		// Decide what to do with contents
		if to == n {
			if od {
				tu.Contents = ""
				tu.Deleted = true
			}
		} else {
			if tpd {
				tu.Contents = ""
				tu.Deleted = true
			}
		}

		// Cut down the contents if they are longer than needed
		if len(tu.Contents) > presets.MaxCont {
			tu.Contents = tu.Contents[0:presets.MaxCont]
		}

		r = append(r, tu)
	}

	var bytes []byte
	bytes, err = json.Marshal(r)

	return bytes

}
