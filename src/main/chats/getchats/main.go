package getchats

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// decode the body
		var req request
		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// is loggedin
		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			w.Write([]byte("notlogged"))
			return
		}

		// get bytes
		bytes := getChats(req.Nickname, conn)
		w.Write(bytes)
	}
	return handle
}
