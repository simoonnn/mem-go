package searchsub

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"log"

	"../../presets"
)

//Row encapsulates an output row info
type Row struct {
	Nickname  string `json:"nickname"`
	AvatarURL string `json:"avatar_url"`
}

func search(requested string, person string, t string, conn *sql.DB) []byte {
	var r []Row
	var tr Row

	// Concatenate %, nickname and %
	var b bytes.Buffer
	b.WriteString("%")
	b.WriteString(requested)
	b.WriteString("%")

	// Choose a fitting query
	var query string
	if t == presets.SUBSCRIBERS {
		query = "SELECT s.subscribed, i.avatar_url FROM subscriptions as s, users_info as i WHERE s.to_person=$1 AND s.subscribed=i.owner AND LOWER(s.subscribed) LIKE LOWER($2) LIMIT $3"
	} else if t == presets.SUBSCRIPTIONS {
		query = "SELECT s.to_person, i.avatar_url FROM subscriptions as s, users_info as i WHERE s.subscribed=$1 AND s.to_person=i.owner AND LOWER(s.to_person) LIKE LOWER($2) LIMIT $3"
	}

	// Select users

	rows, err := conn.Query(query, person, b.String(), presets.GETLIMIT)
	if err != nil {
		log.Println(err)
	} else {
		// Scan through rows
		for rows.Next() {
			err = rows.Scan(&tr.Nickname, &tr.AvatarURL)
			if err != nil {
				log.Println(err)
			} else {
				r = append(r, tr)
			}
		}
	}

	// Return bytes
	var bytes []byte
	bytes, err = json.Marshal(r)

	return bytes
}
