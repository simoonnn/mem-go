package searchsub

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
)

//Request encapsulates request info
type Request struct {
	Requested string `json:"requested"`
	Type      string `json:"type"`
	Person    string `json:"person"`
	Offset    int    `json:"offset"`
}

// Wrapper wraps a function, that searches subscribers/subscriptions
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var response = []byte("[]")

		// Unmarshall the body
		var request Request
		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&request)
		if err != nil {
			log.Println(err)
		} else {
			response = search(request.Requested, request.Person, request.Type, conn)
		}

		w.Write(response)
	}
	return handle
}
