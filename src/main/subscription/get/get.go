package get

import (
	"database/sql"
	"encoding/json"
	"log"

	"../../presets"
	"../searchsub"
)

func get(person string, offset int, t string, conn *sql.DB) []byte {
	var r []searchsub.Row
	var tr searchsub.Row

	// Choose a fitting query
	var query string
	if t == presets.SUBSCRIBERS {
		query = "SELECT s.subscribed, i.avatar_url FROM subscriptions as s, users_info as i WHERE s.to_person=$1 AND s.subscribed=i.owner OFFSET $2 LIMIT $3"
	} else if t == presets.SUBSCRIPTIONS {
		query = "SELECT s.to_person, i.avatar_url FROM subscriptions as s, users_info as i WHERE s.subscribed=$1 AND s.to_person=i.owner OFFSET $2 LIMIT $3"
	}

	// Select users

	rows, err := conn.Query(query, person, offset, presets.GETLIMIT)
	if err != nil {
		log.Println(err)
	} else {
		// Scan through rows
		for rows.Next() {
			err = rows.Scan(&tr.Nickname, &tr.AvatarURL)
			if err != nil {
				log.Println(err)
			} else {
				r = append(r, tr)
			}
		}
		if len(r) == 0 {
			return []byte("[]")
		}
	}

	// Return bytes
	var bytes []byte
	bytes, err = json.Marshal(r)
	if err != nil {
		log.Println(err)
	}

	return bytes
}
