package get

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../searchsub"
)

// Wrapper wraps a function, that searches subscribers/subscriptions
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var response = []byte("[]")

		// Unmarshall the body
		var request searchsub.Request
		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&request)
		if err != nil {
			log.Println(err)
		} else {
			response = get(request.Person, request.Offset, request.Type, conn)
		}

		w.Write(response)
	}
	return handle
}
