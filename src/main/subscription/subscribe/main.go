package subscribe

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type request struct {
	Token    string `json:"token"`
	Nickname string `json:"nickname"`
	Person   string `json:"person"`
}

// Wrapper wraps a function, that subscribes to a user
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var res []byte

		// decode the body
		var req request
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// check if logged in
		if req.Nickname == "" || req.Token == "" {
			res = []byte("notlogged")
			w.Write(res)
			return
		}

		logged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !logged {
			res = []byte("notlogged")
			w.Write(res)
			return
		}

		// subscribe
		subscribe(req.Nickname, req.Token, req.Person, conn)
		res = []byte("success")
		w.Write(res)
	}

	return handle
}
