package subscribe

import (
	"database/sql"
	"log"
)

func subscribe(nick string, t string, p string, c *sql.DB) {
	q := "SELECT FROM subscriptions WHERE subscribed=$1 AND to_person=$2"
	err := c.QueryRow(q, nick, p).Scan()

	// The person is subscribing
	if err == sql.ErrNoRows {
		q = "INSERT INTO subscriptions (subscribed, to_person) VALUES ($1, $2)"
		var stm, err = c.Prepare(q)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(nick, p)
		if err != nil {
			log.Println(err)
		}

		// Update subscribers count
		q = "UPDATE users_info SET subscribers=subscribers + 1 WHERE owner=$1"
		stm, err = c.Prepare(q)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(p)
		if err != nil {
			log.Println(err)
		}

		// Update subscriptions count
		q = "UPDATE users_info SET subscriptions=subscriptions + 1 WHERE owner=$1"
		stm, err = c.Prepare(q)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(nick)
		if err != nil {
			log.Println(err)
		}
	} else {
		// The person is unsubscribing
		q = "DELETE FROM subscriptions VALUES WHERE subscribed=$1 AND to_person=$2"
		var stm, err = c.Prepare(q)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(nick, p)
		if err != nil {
			log.Println(err)
		}

		// Update subscribers count
		q = "UPDATE users_info SET subscribers=subscribers - 1 WHERE owner=$1"
		stm, err = c.Prepare(q)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(p)
		if err != nil {
			log.Println(err)
		}

		// Update subscriptions count
		q = "UPDATE users_info SET subscriptions=subscriptions - 1 WHERE owner=$1"
		stm, err = c.Prepare(q)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(nick)
		if err != nil {
			log.Println(err)
		}
	}
}
