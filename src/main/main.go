package main

import (
	"log"
	"net/http"

	"./login"

	// chats
	"./chats/checkchats"
	"./chats/checkmessages"
	deletemsg "./chats/delmsg"
	"./chats/editmsg"
	getinitialchats "./chats/getchats"
	getinitialmsgs "./chats/getinitial"
	"./chats/getmsgs"
	newmsg "./chats/new"
	"./chats/open"
	"./chats/reply"
	chatsearch "./chats/search"
	"./chats/searchexisting"
	"./chats/wipemsg"

	// posts
	"./posts/deletepost"
	"./posts/getposts"
	"./posts/likepost"
	"./posts/newpost"
	"./posts/sharepost"
	"./posts/singlepost"

	// register
	"./register/activate"
	"./register/checknick"
	"./register/register"

	// subscriptions
	"./subscription/get"
	"./subscription/searchsub"
	"./subscription/subscribe"

	// users
	"./users/exists"
	"./users/info"
	"./users/search"

	// miscellaneous
	"./chats/avatar"
	"./presets"
	"./utils/connect"
	"./utils/headers"

	// avatar
	"./avatar/delavatar"
)

func main() {
	// set up a Database connection
	dbCred := connect.DBCredentials{Username: "simon", Password: "supersimon", DbName: "stuff"}
	conn, err := connect.Connect(dbCred)
	if err != nil {
		log.Println("Failed to connect to the database")
	}

	log.Println(dbCred)
	log.Println("Starting server")

	// send CORS Headers
	methods := "POST, GET"
	allowCredentials := "true"
	origin := presets.FrontEnd
	headerFunc := headers.GetCORSFunction(methods, origin, allowCredentials)

	// users operations
	http.HandleFunc("/register", register.Wrapper(headerFunc, conn))
	http.HandleFunc("/activate", activate.Activate(conn))
	http.HandleFunc("/checkNick", checknick.CheckNick(headerFunc, conn))
	http.HandleFunc("/userexists", exists.Wrapper(headerFunc, conn))
	http.HandleFunc("/getuserinfo", info.Wrapper(headerFunc, conn))
	http.HandleFunc("/searchusers", search.Wrapper(headerFunc, conn))
	http.HandleFunc("/searchsubscriptions", searchsub.Wrapper(headerFunc, conn))
	http.HandleFunc("/getsubscriptions", get.Wrapper(headerFunc, conn))

	// login from browser
	http.HandleFunc("/login", login.Wrapper(headerFunc, presets.TFBrowser, conn))

	// messages and chats
	http.HandleFunc("/editmsg", editmsg.Wrapper(headerFunc, conn))
	http.HandleFunc("/checkformessages", checkmessages.Wrapper(headerFunc, conn))
	http.HandleFunc("/checkforchats", checkchats.Wrapper(headerFunc, conn))
	http.HandleFunc("/replytomsg", reply.Wrapper(headerFunc, conn))
	http.HandleFunc("/openchat", open.Wrapper(headerFunc, conn))
	http.HandleFunc("/deletemsg", deletemsg.Wrapper(headerFunc, conn))
	http.HandleFunc("/wipemsg", wipemsg.Wrapper(headerFunc, conn))
	http.HandleFunc("/getchats", getinitialchats.Wrapper(headerFunc, conn))
	http.HandleFunc("/searchchats", chatsearch.Wrapper(headerFunc, conn))
	http.HandleFunc("/getinitialmsgs", getinitialmsgs.Wrapper(headerFunc, conn))
	http.HandleFunc("/getmsgs", getmsgs.Wrapper(headerFunc, conn))
	http.HandleFunc("/newmessage", newmsg.Wrapper(headerFunc, conn))
	http.HandleFunc("/searchexistingchats", searchexisting.Wrapper(headerFunc, conn))
	http.HandleFunc("/getavatar", avatar.Wrapper(headerFunc, conn))

	// subscribe operations
	http.HandleFunc("/subscribe", subscribe.Wrapper(headerFunc, conn))

	// posts operations
	http.HandleFunc("/getposts", getposts.Wrapper(headerFunc, conn))
	http.HandleFunc("/likepost", likepost.Wrapper(headerFunc, conn))
	http.HandleFunc("/newpost", newpost.Wrapper(headerFunc, conn))
	http.HandleFunc("/deletepost", deletepost.Wrapper(headerFunc, conn))
	http.HandleFunc("/sharepost", sharepost.Wrapper(headerFunc, conn))
	http.HandleFunc("/singlepost", singlepost.Wrapper(headerFunc, conn))

	// avatar operations
	http.HandleFunc("/avatardelete", delavatar.Wrapper(headerFunc, conn))

	log.Fatal(http.ListenAndServe(":9001", nil))
}
