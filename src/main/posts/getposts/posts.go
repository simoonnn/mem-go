package getposts

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
)

type data struct {
	Nickname       string `json:"nickname"`
	Offset         int    `json:"offset"`
	LoggedNickname string `json:"logged"`
}

// Wrapper returns a function to fetch user's posts
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		var data data
		//Send cors headers
		SendCORSHeaders(w)

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&data)
		if err != nil {
			log.Println(err)
		}

		response := getPosts(data.Nickname, data.LoggedNickname, data.Offset, conn)

		w.Write(response)
	}
	return handle
}
