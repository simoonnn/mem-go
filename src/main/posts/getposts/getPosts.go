package getposts

import (
	"database/sql"
	"encoding/json"
	"log"
	"strings"

	"../../presets"
)

// Post encapsulates the post
type Post struct {
	ID       int      `json:"id"`
	Owner    string   `json:"owner"`
	Author   string   `json:"author"`
	Desc     string   `json:"description"`
	Images   []string `json:"images"`
	Contents string   `json:"contents"`
	Likes    int      `json:"likes"`
	Liked    bool     `json:"liked"`
	Date     uint64   `json:"date"`
}

func getPosts(owner string, logged string, offset int, conn *sql.DB) []byte {
	posts := make([]Post, 0, presets.PPR)
	var singlePost Post

	query := "SELECT id, owner, author, images, contents, likes, description, date FROM posts WHERE owner=$1 ORDER BY id DESC OFFSET $2 LIMIT $3"
	rows, err := conn.Query(query, owner, offset, presets.PPR)
	if err != nil {
		log.Println(err)
	}

	var tURLS string
	for rows.Next() {

		err = rows.Scan(&singlePost.ID, &singlePost.Owner, &singlePost.Author, &tURLS, &singlePost.Contents, &singlePost.Likes, &singlePost.Desc, &singlePost.Date)

		if tURLS == "" {
			singlePost.Images = []string{}
		} else {
			singlePost.Images = strings.Split(tURLS, ";")
		}

		if err != nil {
			log.Println(err)
		}

		// Is liked
		query = "SELECT FROM likes WHERE post_id=$1 AND owner=$2"
		err = conn.QueryRow(query, singlePost.ID, logged).Scan()

		// Means the post is liked by the user
		if err == nil {
			singlePost.Liked = true
		} else if err == sql.ErrNoRows /* Means the opposite*/ {
			singlePost.Liked = false
		} else {
			log.Println(err)
			singlePost.Liked = false
		}

		posts = append(posts, singlePost)
	}

	var response []byte

	response, err = json.Marshal(posts)
	if err != nil {
		log.Println(err)
	}

	return response
}
