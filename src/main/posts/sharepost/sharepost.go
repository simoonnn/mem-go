package sharepost

import (
	"database/sql"
	"log"
)

type postdata struct {
	Author   string
	Images   string
	Contents string
}

func sharepost(r request, conn *sql.DB) int {
	var data postdata
	// Get the post info
	query := "SELECT author, images, contents FROM posts WHERE id=$1"
	err := conn.QueryRow(query, r.PostID).Scan(&data.Author, &data.Images, &data.Contents)
	if err != nil {
		log.Println(err)
	}

	// Insert a new post
	var id int
	query = "INSERT INTO posts (owner, images, contents, author, description, date) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
	err = conn.QueryRow(query, r.Nickname, data.Images, data.Contents, data.Author, r.Description, r.Date).Scan(&id)
	if err != nil {
		log.Println(err)
	}
	return id
}
