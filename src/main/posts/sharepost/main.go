package sharepost

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"../../utils/logged"
)

type request struct {
	Nickname    string `json:"nickname"`
	Date        int64  `json:"date"`
	Token       string `json:"token"`
	PostID      int    `json:"post_id"`
	Description string `json:"description"`
}

type response struct {
	Status string `json:"status"`
	ID     int    `json:"id"`
}

//Wrapper wraps function to add new posts to the database
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var res response

		// Decode body
		var req request
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Is logged in?
		il := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if il {
			req.Date = time.Now().UnixNano() / (1000 * 1000)
			res.Status = "success"
			res.ID = sharepost(req, conn)
		} else {
			res.Status = "notlogged"
		}

		var bytes []byte
		bytes, err = json.Marshal(res)

		w.Write(bytes)
	}

	return handle
}
