package deletepost

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type response struct {
	Status string `json:"status"`
}

type data struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	ID       int    `json:"id"`
}

//Wrapper wraps function to add new posts to the database
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		//Initialize response

		var response response
		response.Status = "success"

		SendCORSHeaders(w)

		// Unmarshall the request body
		var data data
		decoder := json.NewDecoder(r.Body)
		decoder.Decode(&data)

		//Check if logged in
		isLogged := logged.IsLoggedIn(data.Nickname, data.Token, conn)
		if !isLogged {
			return
		}

		//Delete the post
		query := "DELETE FROM posts WHERE id=$1 AND owner=$2"
		stm, err := conn.Prepare(query)
		if err != nil {
			response.Status = "server"
			log.Println(err)
		}
		stm.Exec(data.ID, data.Nickname)

		marshalled, _ := json.Marshal(response)

		w.Write(marshalled)
	}
	return handle
}
