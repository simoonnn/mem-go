package likepost

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
)

type request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
	PostID   int    `json:"post_id"`
}

//Wrapper wraps function to add new posts to the database
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		var res []byte
		SendCORSHeaders(w)

		// Decode body
		var req request
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		// Is logged in?
		il := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if il {
			res = likepost(req, conn)
		} else {
			res = []byte("notlogged")
		}

		w.Write(res)
	}

	return handle
}
