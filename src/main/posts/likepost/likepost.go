package likepost

import (
	"database/sql"
	"log"
)

func likepost(r request, conn *sql.DB) []byte {
	var tu interface{}
	var tp interface{}

	query := "SELECT * FROM likes WHERE owner=$1 and post_id=$2"
	err := conn.QueryRow(query, r.Nickname, r.PostID).Scan(&tu, &tp)

	// if user hasn't liked the post
	if err == sql.ErrNoRows {
		query = "INSERT INTO likes (owner, post_id) VALUES($1, $2)"

		var stm, err = conn.Prepare(query)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(r.Nickname, r.PostID)
		if err != nil {
			log.Println(err)
		}

		// Get likes count
		query = "SELECT likes FROM posts WHERE id=$1"
		var lc int
		err = conn.QueryRow(query, r.PostID).Scan(&lc)
		if err != nil {
			log.Println(err)
		}

		// Increment like count
		lc = lc + 1
		query = "UPDATE posts SET likes=$1 WHERE id=$2"

		stm, err = conn.Prepare(query)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(lc, r.PostID)
		if err != nil {
			log.Println(err)
		}

		return []byte("liked")
	} else if err == nil {
		// Delete from likes
		query = "DELETE FROM likes WHERE owner=$1 AND post_id=$2"

		var stm, err = conn.Prepare(query)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(r.Nickname, r.PostID)
		if err != nil {
			log.Println(err)
		}

		// Get likes count
		query = "SELECT likes FROM posts WHERE id=$1"
		var lc int
		err = conn.QueryRow(query, r.PostID).Scan(&lc)
		if err != nil {
			log.Println(err)
		}

		// Decrement like count
		lc = lc - 1
		query = "UPDATE posts SET likes=$1 WHERE id=$2"

		stm, err = conn.Prepare(query)
		if err != nil {
			log.Println(err)
		}
		_, err = stm.Exec(lc, r.PostID)
		if err != nil {
			log.Println(err)
		}

		return []byte("unliked")
	}

	log.Println(err)
	return []byte("error")
}
