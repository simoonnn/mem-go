package singlepost

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../getposts"
)

type request struct {
	ID string `json:"id"`
}

type response struct {
	Status string        `json:"status"`
	Post   getposts.Post `json:"post"`
}

//Wrapper wraps function to add new posts to the database
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		var data request
		var res response

		SendCORSHeaders(w)
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&data)
		if err != nil {
			log.Println(err)
		}

		getPost(data.ID, &res, conn)

		// marshal the response
		var bytes []byte
		bytes, err = json.Marshal(res)
		if err != nil {
			log.Println(err)
		}

		w.Write(bytes)
	}

	return handle
}
