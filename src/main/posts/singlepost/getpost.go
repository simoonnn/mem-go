package singlepost

import (
	"database/sql"
	"strings"

	"../getposts"
)

func getPost(id string, res *response, c *sql.DB) {
	var post getposts.Post
	var tURLS string

	// get the post
	err := c.QueryRow("SELECT id, owner, author, images, contents, likes, description, date FROM posts WHERE id=$1", id).Scan(&post.ID, &post.Owner, &post.Author, &tURLS, &post.Contents, &post.Likes, &post.Desc, &post.Date)

	if tURLS == "" {
		post.Images = []string{}
	} else {
		post.Images = strings.Split(tURLS, ";")
	}

	if err == sql.ErrNoRows {
		res.Status = "norows"
	} else {
		res.Status = "success"
		res.Post = post
	}
}
