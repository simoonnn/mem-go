package newpost

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"

	"../../utils/logged"
)

type body struct {
	Contents string `json:"contents"`
	Owner    string `json:"owner"`
	Token    string `json:"token"`
	Date     int64  `json:"date"`
}

type response struct {
	ID     int    `json:"id"`
	Status string `json:"status"`
}

//Wrapper wraps function to add new posts to the database
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		var resp response
		SendCORSHeaders(w)

		// Decode the request body
		var data body
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&data)
		if err != nil {
			log.Println(err)
		}
		isLogged := logged.IsLoggedIn(data.Owner, data.Token, conn)
		if !isLogged {
			resp.Status = "Not logged in"
		} else {
			// Examine the trimed data
			trimed := strings.TrimSpace(data.Contents)
			resp.ID = -1
			var id int
			if trimed == "" {
				resp.Status = "success"
			} else {
				if len(trimed) > 1200 {
					resp.Status = "toolong"
				} else {
					data.Date = time.Now().UnixNano() / (1000 * 1000)
					id = newPost(data, trimed, conn)
					resp.Status = "success"
				}
			}
			resp.ID = id
		}

		var respBytes []byte

		respBytes, err = json.Marshal(resp)
		w.Write(respBytes)
	}
	return handle
}
