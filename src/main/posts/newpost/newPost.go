package newpost

import (
	"database/sql"
	"log"
)

func newPost(data body, contents string, conn *sql.DB) int {
	query := "INSERT INTO posts (contents, owner, author, date) VALUES ($1, $2, $2, $3) RETURNING id"
	var id int
	err := conn.QueryRow(query, contents, data.Owner, data.Date).Scan(&id)
	if err != nil {
		log.Println(err)
		return -1
	}
	return id
}
