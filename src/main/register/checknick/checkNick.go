package checknick

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"

	"../rowexists"
)

// CheckNick checks if the email is available
func CheckNick(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		nickBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println(err)
		}

		nick := string(nickBytes)
		recordExistsRegistered := rowexists.RowExists(conn, "registered_users", "nickname", nick)
		recordExists := rowexists.RowExists(conn, "users", "nickname", nick)

		if recordExists || recordExistsRegistered {
			w.Write([]byte("exists"))
		} else {
			w.Write([]byte("none"))
		}
	}
	return handle
}
