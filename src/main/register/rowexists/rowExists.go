package rowexists

import (
	"database/sql"
	"fmt"
	"log"
)

func RowExists(conn *sql.DB, tableName string, column string, value interface{}) bool {
	query := fmt.Sprintf("SELECT * FROM %s WHERE %s=$1", tableName, column)
	r, err := conn.Query(query, value)
	if err != nil {
		log.Println("Error in rowExists.go", err)
	}
	rowExists := r.Next()
	return rowExists
}
