package activate

import (
	"bytes"
	"database/sql"
	"log"
	"net/http"

	"../../presets"
	"golang.org/x/crypto/bcrypt"
)

type dbData struct {
	Email              string
	Nickname           string
	PasswordDigest     string
	RegistrationDigest []byte
}

// Activate returns a function that activates an email
func Activate(conn *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		// Get the url of the login page
		var buffer bytes.Buffer
		buffer.WriteString(presets.FrontEnd)
		buffer.WriteString("/#/login")
		url := buffer.String()

		// Get email and activation code
		qValues := r.URL.Query()
		email := qValues.Get("email")
		activationCode := qValues.Get("code")

		// Select a user from registered_users
		var data dbData
		err := conn.QueryRow("SELECT reg_digest, nickname, email, password_digest FROM registered_users WHERE email=$1", email).Scan(&data.RegistrationDigest, &data.Nickname, &data.Email, &data.PasswordDigest)
		// Error indicates the absence of a user
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		// Error indicates that digest and code don't match
		err = bcrypt.CompareHashAndPassword(data.RegistrationDigest, []byte(activationCode))
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		// Add to 'users'
		stm, err := conn.Prepare("INSERT INTO users (email, nickname, password_digest) VALUES ($1, $2, $3)")
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}
		_, err = stm.Exec(email, data.Nickname, data.PasswordDigest)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		//Delete from current database
		stm, err = conn.Prepare("DELETE FROM registered_users WHERE email=$1")
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		_, err = stm.Exec(email)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		// Create a new row in tokens.
		stm, err = conn.Prepare("INSERT INTO tokens (owner) VALUES ($1)")
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		_, err = stm.Exec(data.Nickname)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		// Create a new row in users_info
		stm, err = conn.Prepare("INSERT INTO users_info (owner) VALUES ($1)")
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		_, err = stm.Exec(data.Nickname)
		if err != nil {
			log.Println(err)
			http.Redirect(w, r, presets.FrontEnd, 302)
			return
		}

		http.Redirect(w, r, url, 302)

	}
	return handle
}
