package register

import "./validators"

// IsPOSTDataValid returns whether the post data is valid
func IsPOSTDataValid(data FormData) bool {
	emailValid := validators.Email(data.Email)
	passValid := validators.Password(data.Password)
	nickValid := validators.Nickname(data.Nickname)

	// Data is valid only if everything is valid
	if emailValid && passValid && nickValid {
		return true
	}
	return false
}
