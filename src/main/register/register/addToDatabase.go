package register

import (
	"database/sql"
	"errors"
	"log"
	"strings"

	"../rowexists"
)

const errRegisteredEmailUsed = `pq: duplicate key value violates unique constraint "registered_users_pkey`
const errRegisteredNickUsed = `pq: duplicate key value violates unique constraint "registered_users_nickname_key"`

// AddToDatabase adds a user to the database
func addToDatabase(formData FormData, regDigest string, conn *sql.DB) error {
	// Does row exist?
	var rowExists bool
	if conn == nil {
		log.Println("Conn is nil")
		return errors.New("Conn is nil")
	}
	rowExists = rowexists.RowExists(conn, "users", "email", formData.Email)

	if rowExists {
		return errors.New(errEmailUsed)
	}
	passHash := hashPassword(formData.Password)
	query := "INSERT INTO registered_users(email, password_digest, nickname, reg_digest) VALUES ($1, $2, $3, $4)"
	_, err := conn.Query(query, formData.Email, passHash, formData.Nickname, regDigest)
	if err == nil {
		rowExists = rowexists.RowExists(conn, "users", "nickname", formData.Nickname)

		// If nickname is used...
		if rowExists {
			err = errors.New(errNicknameUsed)
			log.Println("ROWS EXIST NICKNAME")
		}
	} else {
		errMessage := err.Error()

		//Used here, because two errors can occur at the same time. But we want to pick only one.
		if strings.Contains(errMessage, errRegisteredEmailUsed) {
			err = errors.New(errEmailUsed)
		}
		if strings.Contains(errMessage, errRegisteredNickUsed) {
			err = errors.New(errNicknameUsed)
		}
	}
	return err
}
