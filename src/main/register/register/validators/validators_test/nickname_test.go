package validators_test

import (
	"testing"

	"../../validators"
)

func TestNickname(t *testing.T) {
	nicknames := map[string]bool{
		"Sekajdsfj2_l2jk3lkj":  true,
		"<><<>MALICIOUS<><>":   false,
		"Semen2":               true,
		"wow look at me":       false,
		"WOWOWOWO@@#@@#)(*&@#": false,
	}

	for key, value := range nicknames {
		if validators.Nickname(key) != value {
			if value == false {
				t.Error(key, " is asssumed valid")
			} else {
				t.Error(key, " is asssumed invalid")
			}
		}
	}
}
