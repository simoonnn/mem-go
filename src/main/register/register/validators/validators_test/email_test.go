package validators_test

import (
	"testing"

	"../../validators"
)

func TestEmail(t *testing.T) {
	const validEmail = "semen.metelev@gmail.com"
	const invalidEmail = "sdlfkjafjljljj<>><.23+++"

	if validators.Email(validEmail) == false {
		t.Error("Valid email is assumed invalid")
	}

	if validators.Email(invalidEmail) == true {
		t.Error("Invalid Email is assumed valid")
	}
}
