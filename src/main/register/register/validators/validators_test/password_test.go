package validators_test

import (
	"testing"

	"../../validators"
)

func TestPassword(t *testing.T) {
	passes := map[string]bool{
		"Semka228":       true,
		"Semka232><><>":  false,
		"adfafljasfasfj": false,
		"alksd":          false,
		"JKSj32":         false,
	}

	for key, value := range passes {
		if validators.Password(key) != value {
			if value == false {
				t.Error("Invalid password is asssumed valid")
			} else {
				t.Error("Valid password is asssumed invalid")
			}
		}
	}
}
