package validators

import (
	"fmt"
	"regexp"
)

// Password validates password
func Password(psw string) bool {
	// At least 8 characters
	if len(psw) < 8 {
		return false
	}

	passBytes := []byte(psw)

	//One lowercase letter
	match, err := regexp.Match("[a-z]+", passBytes)
	if !match || (err != nil) {
		return false
	}

	//One capital letter
	match, err = regexp.Match("[A-Z]+", passBytes)
	if !match || (err != nil) {
		return false
	}

	//One number
	match, err = regexp.Match("[0-9]+", passBytes)
	if !match || (err != nil) {
		return false
	}

	specialChars := "()<>.{}"
	specialCharsPattern := fmt.Sprintf("[%s]+", specialChars)
	//Escape special chars
	match, _ = regexp.Match(specialCharsPattern, passBytes)
	if match {
		return false
	}

	return true

}
