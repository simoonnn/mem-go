package validators

import "regexp"

// Email validates Email
func Email(email string) bool {
	emailPattern := "[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"
	match, err := regexp.Match(emailPattern, []byte(email))

	if err != nil {
		return false
	}
	return match
}
