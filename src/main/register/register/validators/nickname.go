package validators

import "regexp"

// Nickname validates nickname
func Nickname(nickname string) bool {
	pattern := "^[A-Za-z0-9$_@]+$"
	match, err := regexp.Match(pattern, []byte(nickname))

	if err != nil {
		return false
	}
	return match
}
