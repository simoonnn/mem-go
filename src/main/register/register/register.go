package register

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)

// Constants
var errEmailUsed = `pq: duplicate key value violates unique constraint "users_email_key"`

var errNicknameUsed = `pq: duplicate key value violates unique constraint "users_pkey"`

// Response
type response struct {
	// none=everything is alright, email=email is already used, nickname=nickname is already used, unknown=error is unknown
	ErrorType string `json:"status"`
}

// FormData instances will contain form data sent by the Front-end
type FormData struct {
	Email                string `json:"email"`
	Password             string `json:"pass"`
	PasswordConfirmation string `json:"passConfirm"`
	Nickname             string `json:"nickname"`
}

// Wrapper returns a function to register a user by adding his credentials to database
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		var response response
		var respBytes []byte

		SendCORSHeaders(w)
		var registerData FormData
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&registerData)
		if err != nil {
			log.Println("Could not decode request body")
			return
		}

		// Is request body valid?
		valid := IsPOSTDataValid(registerData)

		// If not valid, don't even bother to do the rest
		if !valid {
			response.ErrorType = "unknown"
			respBytes, err = json.Marshal(response)
			if err != nil {
				log.Println(err)
			}

			//Write the response
			w.Write(respBytes)
			return
		}

		// If passwords don't match, don't even worry about the database
		if registerData.Password != registerData.PasswordConfirmation {
			log.Println("Passwords don't match")
			return
		}
		// Try to add to the database

		// Create registration digest
		randomString := GenerateRandomString()
		regDigestBytes, err := bcrypt.GenerateFromPassword([]byte(randomString), 4)
		if err != nil {
			log.Println("Unable to generate registration digest")
			log.Println(err)
		}
		regDigest := string(regDigestBytes)
		err = addToDatabase(registerData, regDigest, conn)

		//Switch error message
		if err != nil {
			switch err.Error() {
			case errEmailUsed:
				response.ErrorType = "email"
				break
			case errNicknameUsed:
				response.ErrorType = "nickname"
				break
			default:
				response.ErrorType = "unknown"
			}
		} else {
			response.ErrorType = "none"

			//Send confirmation email
			go sendConfirmationEmail(registerData.Email, randomString)
		}

		respBytes, err = json.Marshal(response)
		if err != nil {
			log.Println(err)
		}

		//Write the response
		w.Write(respBytes)
	}
	return handle
}
