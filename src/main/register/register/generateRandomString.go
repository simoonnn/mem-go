package register

import (
	"crypto/md5"
	"fmt"
	"math/rand"
	"strings"
)

// GenerateRandomString generates a random string / hash
func GenerateRandomString() string {
	chars := []rune("qwertyuiopasdfghjknmQWERTYUIPOASDFGHJKZXCVN")

	charsLength := len(chars)
	stringLength := 15

	var b strings.Builder
	for i := 0; i < stringLength; i++ {
		b.WriteRune(chars[rand.Intn(charsLength)])
	}

	hashString := md5.Sum([]byte(b.String()))

	return fmt.Sprintf("%x", hashString)

}
