package register

import (
	"bytes"
	"fmt"
	"log"
	"os"

	"../../presets"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

const emailPlainTemplate = "To activate your account, please follow this link: %s?email=%s&code=%s"

const emailHTMLTemplate = "<html><meta name='viewport' content='width=device-width, initial-scale=1.0'><div style='font-size: 20px;font-family: Arial'>To activate your account, please follow this link: <a style='font-size: 23px;font-family: Impact;font-weight:600;color:#0804f6' href='%s?email=%s&code=%s'>Activate</a><div></html>"

func sendConfirmationEmail(email string, hash string) {
	// Construct activation url
	var buffer bytes.Buffer
	_, fail := buffer.WriteString(presets.BackEnd)
	if fail != nil {
		log.Println(fail)
	}

	_, fail = buffer.WriteString("/activate")
	if fail != nil {
		log.Println(fail)
	}
	address := buffer.String()

	//Main logic
	log.Println("Sending to: ", email)
	from := mail.NewEmail("Example User", "semen.metelev@gmail.com")
	subject := "Account Confirmation"
	to := mail.NewEmail("Dear sir or lady", email)

	// Compose messages
	plainTextContent := fmt.Sprintf(emailPlainTemplate, address, email, hash)
	htmlContent := fmt.Sprintf(emailHTMLTemplate, address, email, hash)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)

	// Send the thing
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
	response, err := client.Send(message)

	// Error handling
	if err != nil {
		log.Println(err)
	} else {
		log.Println(response.StatusCode)
		log.Println(response.Body)
		log.Println(response.Headers)
	}
}
