package register_test

import (
	"database/sql"
	"log"
	"testing"

	"../../../utils/connect"
)

func TestRegister(t *testing.T) {
	var dbCreds = connect.DBCredentials{Username: "simon", Password: "supersimon", DbName: "testing"}
	var conn *sql.DB
	var err error
	conn, err = connect.Connect(dbCreds)
	if err != nil {
		t.Error(err)
	}

	// CREATE TABLE
	_, err = conn.Query("CREATE TABLE users (email VARCHAR(200) UNIQUE, digest VARCHAR(200), nickname VARCHAR(200) PRIMARY KEY)")
	if err != nil {
		t.Error(err)
	}

	// Insert a user
	tE := "hello"
	tD := "WEEL"
	tN := "How are you?"

	_, err = conn.Query("INSERT INTO users (email, digest, nickname) VALUES ($1, $2, $3)", tE, tD, tN)
	if err != nil {
		t.Error(err)
	}

	// Insert a user with the same nick
	tE = "hello23"
	tD = "WEEL"
	tN = "How are you?"

	_, err = conn.Query("INSERT INTO users (email, digest, nickname) VALUES ($1, $2, $3)", tE, tD, tN)
	log.Println(err)

	if err == nil {
		t.Error(err)
	}

	// Insert a user with the same email
	tE = "hello"
	tD = "WEEL"
	tN = "How 232are you?"

	_, err = conn.Query("INSERT INTO users (email, digest, nickname) VALUES ($1, $2, $3)", tE, tD, tN)
	if err == nil {
		t.Error(err)
	}

	// Get rid of the table
	_, err = conn.Query("DROP TABLE users")
	if err != nil {
		t.Error(err)
	}
}
