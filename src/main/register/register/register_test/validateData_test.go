package register_test

import (
	"testing"

	. "../../register"
)

var mockingData = map[FormData]bool{
	FormData{
		Password: "hello_thereSD2",
		Email:    "semen.metelev@gmail.com",
		Nickname: "HelloTHEre1234",
	}: true,
	FormData{
		Password: "hello_2",
		Email:    "semen.metelev@gmail.com",
		Nickname: "HelloTHEre1234",
	}: false,
	FormData{
		Password: "hello_ther@#@#DVeSD2",
		Email:    "semen.metelevgmail.com",
		Nickname: "<>23jLhf",
	}: false,
	FormData{
		Password: "he134134dfdfM<><>llo_thereSD2",
		Email:    "semen.metelev@gmail.com",
		Nickname: "HelloTHEre1234",
	}: false,
	FormData{
		Password: "WowLookAtMe1313__",
		Email:    "aydar.battalov@gmail.com",
		Nickname: "AydarSuperBOSS",
	}: true,
}

func TestMainValidator(t *testing.T) {
	for key, value := range mockingData {
		if IsPOSTDataValid(key) != value {
			if value == true {
				t.Error(key, " is assumed invalid")
			} else {
				t.Error("Invalid data is assumed valid")
			}
		}
	}
}

func BenchmarkValidator(b *testing.B) {
	//Form data to use
	data := FormData{
		Password: "WowLookAtMe1313__",
		Email:    "aydar.battalov@gmail.com",
		Nickname: "AydarSuperBOSS",
	}

	for i := 0; i < b.N; i++ {
		_ = IsPOSTDataValid(data)
	}
}
