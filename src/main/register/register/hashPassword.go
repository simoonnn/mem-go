package register

import (
	"log"

	"../../presets"
	"golang.org/x/crypto/bcrypt"
)

func hashPassword(passwordText string) string {
	cost := presets.Cost
	passBytes, err := bcrypt.GenerateFromPassword([]byte(passwordText), cost)
	if err != nil {
		log.Println(err)
	}
	return string(passBytes)
}
