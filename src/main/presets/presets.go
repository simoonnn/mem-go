package presets

// Cost of the bcrypt password hashing
var Cost = 10

// FrontEnd is the url of the website
var FrontEnd = "http://localhost:8080"

// var FrontEnd = "http://192.168.1.119:8080"

// BackEnd is the url of the Go API
var BackEnd = "http://localhost:9001"

// TFBrowser is the target field name when loggin in from browser
var TFBrowser = "browser_token"

// PPR defines the number of posts per request
var PPR = 7

// SUBSCRIBERS is a constant to handle getting subscribers/subscriptions
var SUBSCRIBERS = "SUBSCRIBERS"

// SUBSCRIPTIONS is a constant to handle getting subscribers/subscriptions
var SUBSCRIPTIONS = "SUBSCRIPTIONS"

// GETLIMIT defines the number of selected subscribers or subscriptions
var GETLIMIT = 40

// MESSAGELIMIT defines the number of messages per request
var MESSAGELIMIT = 30

// CPR defines the number of chats per request
var CPR = 16

// CPRWS defines the number of chats per request while searching
var CPRWS = 20

// MaxCont defines the maximum length of contents string
var MaxCont = 30
