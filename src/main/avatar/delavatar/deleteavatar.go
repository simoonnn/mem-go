package delavatar

import (
	"database/sql"
	"log"

	"../../utils/request"
)

func deleteavatar(req request.Request, c *sql.DB) {
	query := "UPDATE users_info SET avatar_url='' WHERE owner=$1"
	stm, err := c.Prepare(query)
	if err != nil {
		log.Println(err)
	}

	_, err = stm.Exec(req.Nickname)
	if err != nil {
		log.Println(err)
	}
}
