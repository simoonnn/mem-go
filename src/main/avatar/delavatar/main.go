package delavatar

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../../utils/logged"
	"../../utils/request"
)

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var req request.Request

		d := json.NewDecoder(r.Body)
		err := d.Decode(&req)
		if err != nil {
			log.Println(err)
		}

		isLogged := logged.IsLoggedIn(req.Nickname, req.Token, conn)
		if !isLogged {
			w.Write([]byte("notlogged"))
			return
		}

		deleteavatar(req, conn)
		w.Write([]byte("success"))
	}

	return handle
}
