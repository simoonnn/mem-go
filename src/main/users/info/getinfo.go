package info

import (
	"database/sql"
	"encoding/json"
	"log"
)

type dbData struct {
	AvatarURL     string `json:"avatar_url"`
	Subscribers   int    `json:"subscribers"`
	Subscriptions int    `json:"subscriptions"`
	Status        string `json:"status"`
	Subscribed    bool   `json:"subscribed"`
}

func getInfo(n string, o string, conn *sql.DB) []byte {
	var d dbData

	// Select data from users_info
	q := "SELECT avatar_url, subscribers, subscriptions, status FROM users_info WHERE owner=$1"

	err := conn.QueryRow(q, n).Scan(&d.AvatarURL, &d.Subscribers, &d.Subscriptions, &d.Status)
	if err != nil {
		log.Println(err)
	}

	// Check if subscribed to the user
	q = "SELECT FROM subscriptions WHERE subscribed=$1 AND to_person=$2"
	err = conn.QueryRow(q, o, n).Scan()
	if err == sql.ErrNoRows {
		d.Subscribed = false
	} else if err == nil {
		d.Subscribed = true
	} else {
		d.Subscribed = false
		log.Println(err)
	}

	// Marshal the data
	var bs []byte
	bs, err = json.Marshal(d)
	if err != nil {
		log.Println(err)
	}

	return bs
}
