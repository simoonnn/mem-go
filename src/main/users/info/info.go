package info

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
)

type request struct {
	Nickname string `json:"nickname"`
	Owner    string `json:"current"`
}

// Wrapper wrapps a handler function
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		var n request
		d := json.NewDecoder(r.Body)

		err := d.Decode(&n)
		if err != nil {
			log.Println(err)
		}

		data := getInfo(n.Nickname, n.Owner, conn)
		w.Write(data)

	}
	return handle
}
