package exists

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
)

type request struct {
	Nickname string `json:"nickname"`
}

type response struct {
	Exists bool `json:"exists"`
}

// Wrapper wraps a function, that checks if a user exists
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		var req request
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&req)

		exists := doesExist(req.Nickname, conn)

		var res response
		res.Exists = exists

		bytes, err := json.Marshal(res)
		if err != nil {
			log.Println(err)
		}

		w.Write(bytes)

	}

	return handle
}
