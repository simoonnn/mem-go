package exists

import (
	"database/sql"
)

func doesExist(nickname string, conn *sql.DB) bool {
	query := "SELECT nickname FROM users WHERE nickname=$1"

	err := conn.QueryRow(query, nickname).Scan()
	if err == sql.ErrNoRows {
		return false
	}

	return true
}
