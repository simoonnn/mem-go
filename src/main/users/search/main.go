package search

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
)

type request struct {
	Requested string `json:"requested"`
}

// Wrapper returns a function search users
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)
		var response = []byte("[]")

		// Unmarshall the body
		var request request
		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&request)
		if err != nil {
			log.Println(err)
		} else {
			response = searchUsers(request.Requested, conn)
		}

		w.Write(response)
	}
	return handle
}
