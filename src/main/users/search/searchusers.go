package search

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"log"
)

type row struct {
	Nickname  string `json:"nickname"`
	AvatarURL string `json:"avatar_url"`
}

func searchUsers(requested string, conn *sql.DB) []byte {
	var r []row
	var tr row

	// Concatenate %, nickname and %
	var b bytes.Buffer
	b.WriteString("%")
	b.WriteString(requested)
	b.WriteString("%")

	// Select users
	query := "SELECT u.nickname, i.avatar_url FROM users as u, users_info as i WHERE LOWER(u.nickname) LIKE LOWER($1) AND u.nickname=i.owner"

	rows, err := conn.Query(query, b.String())
	if err != nil {
		log.Println(err)
	} else {
		// Scan through rows
		for rows.Next() {
			err = rows.Scan(&tr.Nickname, &tr.AvatarURL)
			if err != nil {
				log.Println(err)
			} else {
				r = append(r, tr)
			}
		}
	}

	// Return bytes
	var bytes []byte
	bytes, err = json.Marshal(r)

	return bytes
}
