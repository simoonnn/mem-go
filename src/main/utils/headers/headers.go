package headers

import "net/http"

// GetCORSFunction returns a function that sends the necessary headers
func GetCORSFunction(methods string,
	origin string, allowCredentials string) func(w http.ResponseWriter) {
	main := func(w http.ResponseWriter) {
		headers := w.Header()
		headers.Add("Access-Control-Allow-Origin", origin)
		headers.Add("Access-Control-Allow-Methods", methods)
		headers.Add("Access-Control-Allow-Credentials", allowCredentials)
	}
	return main

}
