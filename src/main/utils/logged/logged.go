package logged

import (
	"database/sql"
	"log"

	"golang.org/x/crypto/bcrypt"
)

//IsLoggedIn checks if the user is logged in or not
func IsLoggedIn(nickname string, token string, conn *sql.DB) bool {
	var digest string

	// Get the browser_token
	query := "SELECT browser_token FROM tokens WHERE owner=$1"
	err := conn.QueryRow(query, nickname).Scan(&digest)
	if err == sql.ErrNoRows || digest == "" {
		//Get the app_token
		query = "SELECT app_token FROM tokens WHERE owner=$1"
		err := conn.QueryRow(query, nickname).Scan(&digest)
		if err == sql.ErrNoRows {
			return false
		}

		err = bcrypt.CompareHashAndPassword([]byte(digest), []byte(token))
		if err != nil {
			log.Println(err)
			return false
		}
	} else {
		err = bcrypt.CompareHashAndPassword([]byte(digest), []byte(token))
		if err != nil {
			log.Println(err)
			return false
		}
	}

	return true
}
