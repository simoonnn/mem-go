package connect

import (
	"database/sql"
	"fmt"

	//postgresql driver
	_ "github.com/lib/pq"
)

//DBCredentials encapsulates database credentials
type DBCredentials struct {
	Username string
	Password string
	DbName   string
}

// Connect connects to the database and returns the connection object.
func Connect(creds DBCredentials) (*sql.DB, error) {
	dataSourceName := fmt.Sprintf("user=%s password=%s dbname=%s", creds.Username, creds.Password, creds.DbName)
	conn, err := sql.Open("postgres", dataSourceName)
	return conn, err
}
