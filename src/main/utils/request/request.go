package request

// Request represents general request
type Request struct {
	Nickname string `json:"nickname"`
	Token    string `json:"token"`
}
