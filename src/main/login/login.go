package login

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"../register/register"
	"golang.org/x/crypto/bcrypt"
)

type response struct {
	Status       string `json:"status,success"`
	Nickname     string `json:"nickname"`
	SessionToken string `json:"session_token"`
}

type postData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type dbData struct {
	Email          string
	PasswordDigest []byte
	Nickname       string
}

/*Wrapper wraps an http handler. The 'field' variable defines which field to update
 */
func Wrapper(SendCORSHeaders func(w http.ResponseWriter), field string, conn *sql.DB) func(http.ResponseWriter, *http.Request) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		SendCORSHeaders(w)

		// Initialize the response object
		var response response

		// Unmarshal POST body
		var data postData
		decoder := json.NewDecoder(r.Body)
		decoder.Decode(&data)

		// Fetch the user from the database
		var dbData dbData
		query := "SELECT password_digest, email, nickname FROM users WHERE email=$1"
		err := conn.QueryRow(query, data.Email).Scan(&dbData.PasswordDigest, &dbData.Email, &dbData.Nickname)
		if err == sql.ErrNoRows {
			response.Status = "creds"
		} else if err != nil {
			log.Println(err)
			response.Status = "server"
		} else {
			// Compare password
			err = bcrypt.CompareHashAndPassword(dbData.PasswordDigest, []byte(data.Password))
			if err != nil {
				log.Println(err)
				response.Status = "creds"
			} else {

				hash := register.GenerateRandomString()
				response.Status = "success"
				response.SessionToken = hash
				response.Nickname = dbData.Nickname
				updateDBToken(hash, field, dbData.Nickname, conn)
			}
		}

		// Encode and send the body
		var body []byte
		body, err = json.Marshal(response)
		if err != nil {
			log.Println(err)
		}
		w.Write(body)
	}
	return handle
}
