package login

import (
	"database/sql"
	"fmt"
	"log"

	"golang.org/x/crypto/bcrypt"
)

func updateDBToken(token string, tf string, nickname string, conn *sql.DB) {
	digest, err := bcrypt.GenerateFromPassword([]byte(token), 5)
	query := fmt.Sprintf("UPDATE tokens SET %s=$1 WHERE owner=$2", tf)
	var stm *sql.Stmt
	stm, err = conn.Prepare(query)
	if err != nil {
		log.Println(err)
	}
	_, err = stm.Exec(digest, nickname)
	if err != nil {
		log.Println(err)
	}
}
